# Install kubectx
Install kubectx, kubens, and oh-my-zsh autocompletion scripts.

## Role Variables
### Defaults
| Variable        | Default | Description                                                                                 |
|-----------------|---------|---------------------------------------------------------------------------------------------|
| kubectx_version | HEAD    | What version to check out. This can be the literal string  HEAD, a branch name, a tag name. |

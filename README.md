# Ansible
A playbook meant to customize an environment in a standard way from scratch to my liking.

## `setup.yml`
Bootstrap a Fedora 32 system from scratch to a full configuration with an account, ssh keys, preferred packages, and dotfiles.

### Notes
- Run with `sudo`
